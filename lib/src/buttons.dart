import 'package:flutter/material.dart';

/// button types
enum T { primary, secondary, errorPrimary, errorSecondary }

/// buttons
class Button {
  Widget style(T type, Function? onPressed) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: active ? onPressed : null,
      child: Container(
        decoration: BoxDecoration(
          color: active ? AppColors.White : AppColors.Gray,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: CustomColors.primaryColor,
            width: 2.0,
          ),
        ),
        margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
        width: double.infinity,
        height: 50,
        child: Center(
          child: Text(
            text,
            style: const TextStyle(
              fontSize: 16,
              color: AppColors.primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
