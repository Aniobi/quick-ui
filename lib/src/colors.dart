import 'package:flutter/material.dart' as w;

/// Color types
enum CT {
  primary,
  secondary,
  error,
  black,
  white,
  background,
}

class CustomColors {
  static const bgScaffoldColor = w.Color(0xFFF3F3F3);
  static const componentBgColor = w.Color(0xFFF3F3F3);
  static const componentBgSecondaryColor = w.Colors.grey;
  static const textColor = w.Color(0xFF454545);
  static const textLiteColor = w.Color(0xFF777777);
  static const primaryColor = w.Color(0xFF172f5d);
  static const primaryLightColor = w.Color(0xFFFAA120);
  static const secondaryColor = w.Color(0xFFc47d7b);
  static const secondaryLiteColor = w.Color(0x50A945EA);
}
