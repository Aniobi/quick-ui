
[QuickUi](https://gitlab.com/Aniobi/quick-ui) is a rapid ui development kit for building reusble user interfaces. For android application, this ui-kit is built for [Insurance]() application but is also applicable to any other flutter application.

## Features

| name | type| description|
|------|-----|--------------|
| color | CustomColors | colors used for the application | 
| button | `Buttons` | different button types |
| text styles | `TextStyles` | a TextStyle class for selecting styles. This is used for the `Text` widget |

## Getting started

install ui kit in `pubspec.yaml` under *dependencies* 
```yaml
dependencies:
  quick_ui:
    git:
      url: https://gitlab.com/Aniobi/quick-ui
      ref: main
```

Import the package in the file using command below
```dart
import 'package:quick_ui';
```

Tryout Ui kit
```dart
Text(
    'example text',
    style: TextStyles.styles(H.h1, B.b1, S.roboto)
)
```
The `Text` widget makes use of the flutter widget.

## Usage

**Design kits Available :**
- [x] TextStyling
- [x] Custom Colors
- [ ] Buttons 
- [ ] TextFields
- [ ] Checkboxes

### Font Styles
```dart
Text(
    'example text',
    style: TextStyles.styles(H.h1, B.b1, S.roboto)
)
```

### Colors
```dart
CustomColors.primary // default blue color
```


## Additional information

This Ui kit works in conjunction with the default flutter widgets.
